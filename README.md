# vcpkg1

## note
```bash
# 1
cmake --preset=default

# 2 
cmake --build build

# 3
vcpkg add port spdlog
```

## problem
```bash
# openssl build 依赖
sudo dnf install perl-IPC-Cmd
sudo apt install libipc-cmd-perl
```

## prepare
```bash
git clone https://github.com/microsoft/vcpkg.git
cd vcpkg && ./bootstrap-vcpkg.sh
export VCPKG_ROOT=/home/jicheng.tang/vcpkg
export PATH=$VCPKG_ROOT:$PATH
```

## start
```bash
vcpkg new --application
vcpkg add port fmt
```

follow this link:
https://learn.microsoft.com/zh-cn/vcpkg/get_started/get-started?pivots=shell-bash

